1. Purpose of this project is to explore different libraries such as Compose, Hilt, View Model, Navigation and Kotlin Serialization.

2. UI is simple and keep to a minimum with loader and error states.

3. Ideally I would have liked to cache backend response, because flavors do not change a lot.

4. I've added fake order and order response to simulate real world scenario.
