package com.raimbekov.pizza.feature.order.domain

import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.order.domain.repository.OrderRepository
import com.raimbekov.pizza.feature.order.model.Order
import com.raimbekov.pizza.feature.pizza.domain.PizzaInteractor
import com.raimbekov.pizza.feature.pizza.model.Flavor
import com.raimbekov.pizza.feature.pizza.model.Pizza
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.kotlin.any
import org.mockito.kotlin.anyOrNull
import org.mockito.kotlin.whenever

class OrderInteractorImplTest {

    private val pizzaInteractor: PizzaInteractor = mock()
    private val orderRepository: OrderRepository = mock()

    private val interactor: OrderInteractor = OrderInteractorImpl(
        pizzaInteractor = pizzaInteractor,
        orderRepository = orderRepository
    )

    @Test
    fun `GIVEN single flavor pizza WHEN orderPizza THEN order pizza and return order details`() {
        val pizza = Pizza.SingleFlavor(Flavor("Cheese", Money(1000)))
        whenever(pizzaInteractor.getTotalPrice(any(), anyOrNull(), anyOrNull())).thenReturn(
            Money(
                1000
            )
        )
        whenever(orderRepository.order(any(), any())).thenReturn(
            Order(
                "orderId",
                pizza,
                Money(1000)
            )
        )

        val order = interactor.orderPizza(pizza)

        assertEquals(
            Order("orderId", pizza, Money(1000)),
            order
        )
    }

    @Test
    fun `GIVEN two flavor pizza WHEN orderPizza THEN order pizza and return order details`() {
        val pizza = Pizza.TwoFlavor(
            firstFlavor = Flavor("Cheese", Money(1000)),
            secondFlavor = Flavor("Mushroom", Money(1200)),
        )
        whenever(pizzaInteractor.getTotalPrice(any(), anyOrNull(), anyOrNull())).thenReturn(
            Money(
                1100
            )
        )
        whenever(orderRepository.order(any(), any())).thenReturn(
            Order(
                "orderId",
                pizza,
                Money(1100)
            )
        )

        val order = interactor.orderPizza(pizza)

        assertEquals(
            Order("orderId", pizza, Money(1100)),
            order
        )
    }
}