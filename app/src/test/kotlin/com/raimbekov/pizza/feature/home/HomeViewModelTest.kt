package com.raimbekov.pizza.feature.home

import androidx.lifecycle.SavedStateHandle
import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.order.domain.OrderInteractor
import com.raimbekov.pizza.feature.order.model.Order
import com.raimbekov.pizza.feature.pizza.domain.PizzaInteractor
import com.raimbekov.pizza.feature.pizza.model.Flavor
import com.raimbekov.pizza.feature.pizza.model.Pizza
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.kotlin.any
import org.mockito.kotlin.anyOrNull
import org.mockito.kotlin.whenever


class HomeViewModelTest {
    private val pizzaInteractor: PizzaInteractor = mock()
    private val orderInteractor: OrderInteractor = mock()
    private val savedStateHandle: SavedStateHandle = mock()

    private val homeViewModel = HomeViewModel(
        pizzaInteractor = pizzaInteractor,
        orderInteractor = orderInteractor,
        savedStateHandle = savedStateHandle
    )

    @Before
    fun setUp() {
        whenever(pizzaInteractor.getTotalPrice(any(), anyOrNull(), anyOrNull())).thenReturn(
            Money(
                100
            )
        )
    }

    @Test
    fun `GIVEN home screen WHEN set to two flavor pizza THEN twoFlavor is true`() {
        homeViewModel.setTwoFlavorTypeSelected(true)

        assertTrue(homeViewModel.homeUiState.value.twoFlavor)
    }

    @Test
    fun `GIVEN home screen WHEN set first flavor THEN first flavor is update`() {
        homeViewModel.setFirstFlavor(Flavor("Test", Money(100)))

        assertEquals(Flavor("Test", Money(100)), homeViewModel.homeUiState.value.firstFlavor)
    }

    @Test
    fun `GIVEN home screen WHEN set second flavor THEN second flavor is update`() {
        homeViewModel.setSecondFlavor(Flavor("Test", Money(100)))

        assertEquals(Flavor("Test", Money(100)), homeViewModel.homeUiState.value.secondFlavor)
    }

    @Test
    fun `GIVEN home screen WHEN flavors are correct THEN order is enabled`() {
        homeViewModel.setFirstFlavor(Flavor("Test", Money(100)))

        assertTrue(homeViewModel.homeUiState.value.orderEnabled)
    }

    @Test
    fun `WHEN order THEN return order id`() {
        val flavor = Flavor("Test", Money(100))
        val pizza = Pizza.SingleFlavor(flavor)
        val order = Order("orderId", pizza, Money(100))
        whenever(orderInteractor.orderPizza(any())).thenReturn(order)

        homeViewModel.setFirstFlavor(flavor)

        assertEquals("orderId", homeViewModel.order())
    }
}