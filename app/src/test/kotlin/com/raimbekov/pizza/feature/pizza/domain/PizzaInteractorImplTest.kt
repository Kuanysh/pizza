package com.raimbekov.pizza.feature.pizza.domain

import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.pizza.domain.repository.PizzaRepository
import com.raimbekov.pizza.feature.pizza.model.Flavor
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock

internal class PizzaInteractorImplTest {

    private val pizzaRepository: PizzaRepository = mock()
    private val interactor: PizzaInteractor = PizzaInteractorImpl(
        pizzaRepository = pizzaRepository
    )

    @Test
    fun `GIVEN two flavors of pizza WHEN get total price THEN return the average of prices`() {
        val totalPrice = interactor.getTotalPrice(
            twoFlavors = true,
            first = Flavor("First", Money(1000)),
            second = Flavor("Second", Money(2000))
        )

        assertEquals(Money(1500), totalPrice)
    }

    @Test
    fun `GIVEN single flavor pizza WHEN get total price THEN return same price`() {
        val totalPrice = interactor.getTotalPrice(
            twoFlavors = false,
            first = Flavor("First", Money(1000)),
            second = null
        )

        assertEquals(Money(1000), totalPrice)
    }
}