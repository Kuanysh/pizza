package com.raimbekov.pizza.feature.money.model

import junit.framework.TestCase.assertEquals
import org.junit.Test

class MoneyTest {

    @Test
    fun `GIVEN instance of Money THEN currency is always USD`() {
        assertEquals(Currency.USD, Money(100).currency)
    }

    @Test
    fun `GIVEN another money instance WHEN plus THEN return the sum`() {
        val sum = Money(100) + Money(200)
        assertEquals(Money(300), sum)
    }

    @Test
    fun `GIVEN divisor value WHEN divide THEN return the quotient`() {
        val quotient = Money(100) / 2
        assertEquals(Money(50), quotient)
    }

    @Test
    fun `GIVEN Money WHEN print THEN return string representation with symbol`() {
        val money = Money(100)
        assertEquals("$1.0", money.print())
    }
}