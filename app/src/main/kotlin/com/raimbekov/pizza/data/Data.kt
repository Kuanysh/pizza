package com.raimbekov.pizza.data

sealed class Data<out T : Any> {
    data class Loading<out T : Any>(val cache: T? = null) : Data<T>()
    data class Error(val throwable: Exception) : Data<Nothing>()
    data class Content<out T : Any>(val content: T) : Data<T>()
}