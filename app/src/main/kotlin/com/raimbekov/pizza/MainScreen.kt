package com.raimbekov.pizza

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import com.raimbekov.pizza.feature.home.HomeScreen
import com.raimbekov.pizza.feature.home.HomeViewModel
import com.raimbekov.pizza.feature.pizza.model.Flavor
import com.raimbekov.pizza.feature.result.ResultScreen
import com.raimbekov.pizza.feature.result.ResultViewModel
import com.raimbekov.pizza.feature.selection.FlavorSelectionScreen
import com.raimbekov.pizza.feature.selection.SelectionResult
import com.raimbekov.pizza.feature.selection.SelectionViewModel

@Composable
fun MainScreen() {
    val navController = rememberNavController()
    PizzaNavHost(
        navController = navController
    )
}

@Composable
fun PizzaNavHost(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = "order"
    ) {
        navigation(startDestination = "home", route = "order") {
            composable("home") {
                val viewModel = hiltViewModel<HomeViewModel>()
                observeSelectionResults(it, viewModel)
                HomeScreen(
                    viewModel,
                    onFirstFlavorSelectClicked = { navController.navigate("selection/first") },
                    onSecondFlavorSelectClicked = { navController.navigate("selection/second") },
                    onOrderClicked = { orderId ->
                        navController.navigate("result/${orderId}")
                    }
                )
            }
            composable("selection/{resultKey}") {
                val viewModel = hiltViewModel<SelectionViewModel>()
                val resultKey = it.arguments?.getString("resultKey").orEmpty()
                FlavorSelectionScreen(
                    viewModel = viewModel,
                    onFlavorSelected = { flavor ->
                        navController.previousBackStackEntry
                            ?.savedStateHandle
                            ?.set(resultKey, SelectionResult(flavor))
                        navController.popBackStack()
                    }
                )
            }
            composable(
                "result/{orderId}"
            ) {
                val viewModel = hiltViewModel<ResultViewModel>()
                val orderId = checkNotNull(it.arguments?.getString("orderId"))
                it.savedStateHandle["orderId"] = orderId
                ResultScreen(
                    viewModel,
                    onBackClicked = { navController.navigate("order") { popUpTo("home") } }
                )
            }
        }
    }
}

@Composable
private fun observeSelectionResults(
    it: NavBackStackEntry,
    viewModel: HomeViewModel
) {
    it.observeScreenResult(key = "first") { flavor -> viewModel.setFirstFlavor(flavor) }
    it.observeScreenResult(key = "second") { flavor -> viewModel.setSecondFlavor(flavor) }
}

@Composable
private fun NavBackStackEntry.observeScreenResult(key: String, onResult: (Flavor) -> Unit) {
    val selectionResult =
        savedStateHandle.getStateFlow(key, SelectionResult()).collectAsState().value
    savedStateHandle.remove<SelectionResult>(key)
    selectionResult.flavor?.let {
        onResult(it)
    }
}