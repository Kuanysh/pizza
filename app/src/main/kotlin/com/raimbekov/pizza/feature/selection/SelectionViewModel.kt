package com.raimbekov.pizza.feature.selection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raimbekov.pizza.data.Data
import com.raimbekov.pizza.feature.pizza.domain.PizzaInteractor
import com.raimbekov.pizza.feature.pizza.model.Flavor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SelectionViewModel @Inject constructor(
    private val pizzaInteractor: PizzaInteractor
) : ViewModel() {

    val flavors = MutableStateFlow<Data<List<Flavor>>>(Data.Loading())

    init {
        load()
    }

    fun reload() {
        load()
    }

    private fun load() {
        viewModelScope.launch {
            pizzaInteractor.observeFlavors()
                .collectLatest { flavors.value = it }
        }
    }
}