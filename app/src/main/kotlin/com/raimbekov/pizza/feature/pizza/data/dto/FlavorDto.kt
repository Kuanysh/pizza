package com.raimbekov.pizza.feature.pizza.data.dto

import com.raimbekov.pizza.feature.money.data.dto.MoneyDto
import com.raimbekov.pizza.feature.money.data.dto.toDomain
import com.raimbekov.pizza.feature.pizza.model.Flavor
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlavorDto(
    @SerialName("name")
    val name: String,

    @SerialName("price")
    val price: MoneyDto
)

fun FlavorDto.toDomain(): Flavor =
    Flavor(
        name = name,
        price = price.toDomain()
    )