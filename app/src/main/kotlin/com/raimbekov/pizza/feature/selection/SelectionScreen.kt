package com.raimbekov.pizza.feature.selection

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import com.raimbekov.pizza.data.Data
import com.raimbekov.pizza.feature.pizza.model.Flavor

@Composable
fun FlavorSelectionScreen(
    viewModel: SelectionViewModel,
    onFlavorSelected: (Flavor) -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar {
                Row(Modifier.padding(16.dp)) {
                    Text(text = "Select flavor")
                }
            }
        },
        content = FlavorSelectionScreenContent(viewModel, onFlavorSelected)
    )
}

@Composable
private fun FlavorSelectionScreenContent(
    viewModel: SelectionViewModel,
    onFlavorSelected: (Flavor) -> Unit
): @Composable (PaddingValues) -> Unit = {
    when (val flavors: Data<List<Flavor>> = viewModel.flavors.collectAsState().value) {
        is Data.Content -> FlavorsList(flavors.content, onFlavorSelected)
        is Data.Error -> Button(onClick = { viewModel.reload() }) {
            Text(text = "Error. Try again!")
        }
        is Data.Loading -> Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(
                modifier = Modifier.size(48.dp)
            )
        }
    }
}

@Composable
private fun FlavorsList(flavors: List<Flavor>, onFlavorSelected: (Flavor) -> Unit) {
    LazyColumn(
        content = {
            items(flavors.size) { index ->
                ClickableText(
                    text = AnnotatedString(flavors[index].name + " " + flavors[index].price.print()),
                    modifier = Modifier.padding(16.dp),
                    onClick = { onFlavorSelected(flavors[index]) }
                )
            }
        }
    )
}
