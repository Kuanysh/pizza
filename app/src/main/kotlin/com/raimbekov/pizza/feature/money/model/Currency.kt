package com.raimbekov.pizza.feature.money.model

enum class Currency(val subunit: Int) {
    USD(100)
}