package com.raimbekov.pizza.feature.pizza.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

sealed class Pizza : Parcelable {
    @Parcelize
    data class SingleFlavor(
        val flavor: Flavor
    ) : Pizza()

    @Parcelize
    data class TwoFlavor(
        val firstFlavor: Flavor,
        val secondFlavor: Flavor,
    ) : Pizza()
}