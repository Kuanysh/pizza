package com.raimbekov.pizza.feature.order.domain.repository

import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.order.model.Order
import com.raimbekov.pizza.feature.pizza.model.Pizza
import java.util.*
import javax.inject.Inject

interface OrderRepository {
    fun order(pizza: Pizza, price: Money): Order
    fun getOrderDetails(orderId: String): Order
}

class OrderRepositoryImpl @Inject constructor() : OrderRepository {
    private val orders = HashMap<String, Order>()

    override fun order(pizza: Pizza, price: Money): Order {
        val uuid = UUID.randomUUID()
        val order = Order(uuid.toString(), pizza, price)
        orders[order.orderId] = order
        return order
    }

    override fun getOrderDetails(orderId: String): Order = orders[orderId]!!

}