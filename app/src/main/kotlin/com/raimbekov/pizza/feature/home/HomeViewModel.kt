package com.raimbekov.pizza.feature.home

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.raimbekov.pizza.feature.order.domain.OrderInteractor
import com.raimbekov.pizza.feature.pizza.domain.PizzaInteractor
import com.raimbekov.pizza.feature.pizza.model.Flavor
import com.raimbekov.pizza.feature.pizza.model.Pizza
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val pizzaInteractor: PizzaInteractor,
    private val orderInteractor: OrderInteractor,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _homeUiState = MutableStateFlow(HomeUiState())
    val homeUiState: StateFlow<HomeUiState> = _homeUiState

    fun setTwoFlavorTypeSelected(selected: Boolean) {
        updateState { copy(twoFlavor = selected) }
        updateOrderEnabled()
        updateTotalPrice()
    }

    fun setFirstFlavor(flavor: Flavor) {
        updateState { copy(firstFlavor = flavor) }
        updateOrderEnabled()
        updateTotalPrice()
    }

    fun setSecondFlavor(flavor: Flavor) {
        updateState { copy(secondFlavor = flavor) }
        updateOrderEnabled()
        updateTotalPrice()
    }

    fun order(): String {
        val homeUiState = homeUiState.value
        val pizza = if (homeUiState.twoFlavor) {
            Pizza.TwoFlavor(homeUiState.firstFlavor!!, homeUiState.secondFlavor!!)
        } else {
            Pizza.SingleFlavor(homeUiState.firstFlavor!!)
        }
        return orderInteractor.orderPizza(pizza).orderId
    }

    private fun updateOrderEnabled() {
        updateState {
            val enabled = if (twoFlavor) {
                firstFlavor != null && secondFlavor != null
            } else {
                firstFlavor != null
            }
            copy(orderEnabled = enabled)
        }
    }

    private fun updateTotalPrice() {
        updateState {
            copy(totalPrice = pizzaInteractor.getTotalPrice(twoFlavor, firstFlavor, secondFlavor))
        }
    }

    private fun updateState(action: HomeUiState.() -> HomeUiState) {
        _homeUiState.value = action(_homeUiState.value)
    }
}