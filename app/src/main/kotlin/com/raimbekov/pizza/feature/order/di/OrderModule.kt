package com.raimbekov.pizza.feature.order.di

import com.raimbekov.pizza.feature.order.domain.OrderInteractor
import com.raimbekov.pizza.feature.order.domain.OrderInteractorImpl
import com.raimbekov.pizza.feature.order.domain.repository.OrderRepository
import com.raimbekov.pizza.feature.order.domain.repository.OrderRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
interface OrderModule {

    @Singleton
    @Binds
    fun bindOrderInteractor(interactor: OrderInteractorImpl): OrderInteractor

    @Singleton
    @Binds
    fun bindOrderRepository(repository: OrderRepositoryImpl): OrderRepository
}