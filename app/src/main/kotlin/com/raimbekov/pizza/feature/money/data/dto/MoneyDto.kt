package com.raimbekov.pizza.feature.money.data.dto

import com.raimbekov.pizza.feature.money.model.Money
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable(with = MoneyDtoSerializer::class)
@SerialName("MoneyDto")
data class MoneyDto(
    val unit: Long
)

fun MoneyDto.toDomain(): Money =
    Money(unit = unit)