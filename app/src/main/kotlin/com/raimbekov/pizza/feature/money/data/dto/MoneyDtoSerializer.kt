package com.raimbekov.pizza.feature.money.data.dto

import com.raimbekov.pizza.feature.money.model.Currency
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object MoneyDtoSerializer : KSerializer<MoneyDto> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("MoneyDto", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: MoneyDto) {
        val string = (value.unit.toDouble() / Currency.USD.subunit).toString()
        encoder.encodeString(string)
    }

    override fun deserialize(decoder: Decoder): MoneyDto {
        val unitD = decoder.decodeString().toDouble()
        return MoneyDto(unit = (unitD * Currency.USD.subunit).toLong())
    }
}