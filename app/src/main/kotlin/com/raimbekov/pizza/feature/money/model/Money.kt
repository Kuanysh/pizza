package com.raimbekov.pizza.feature.money.model

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Money(
    val unit: Long
) : Parcelable {

    @IgnoredOnParcel
    val currency: Currency = Currency.USD

    operator fun plus(price: Money): Money =
        Money(unit + price.unit)

    operator fun div(divisor: Int): Money =
        Money(unit / divisor)

    fun print(): String =
        when (currency) {
            Currency.USD -> "$${(unit.toDouble() / currency.subunit)}"
        }
}