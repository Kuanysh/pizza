package com.raimbekov.pizza.feature.result

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.pizza.model.Pizza

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ResultScreen(
    resultViewModel: ResultViewModel,
    onBackClicked: () -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar {
                Row(Modifier.padding(16.dp)) {
                    Text(text = "Order receipt")
                }
            }
        }
    ) {
        val order = resultViewModel.getOrder()
        ResultScreenContent(
            pizza = order.pizza,
            price = order.price,
            onBackClicked = onBackClicked
        )
    }
}

@Composable
fun ResultScreenContent(
    pizza: Pizza,
    price: Money,
    onBackClicked: () -> Unit
) {
    Column {
        Text(text = "Order of ${price.print()} is successful")
        Text(text = "Pizza Type:")
        when (pizza) {
            is Pizza.SingleFlavor -> Text(text = pizza.flavor.name)
            is Pizza.TwoFlavor -> Text(text = pizza.firstFlavor.name + " + " + pizza.secondFlavor.name)
        }
        Spacer(modifier = Modifier.height(16.dp))
        Button(onClick = { onBackClicked() }) {
            Text(text = "Go back")
        }
    }
}