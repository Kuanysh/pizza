package com.raimbekov.pizza.feature.pizza.di

import com.raimbekov.pizza.data.network.ktorHttpClient
import com.raimbekov.pizza.feature.pizza.data.PizzaService
import com.raimbekov.pizza.feature.pizza.data.PizzaServiceImpl
import com.raimbekov.pizza.feature.pizza.data.repository.PizzaRepositoryImpl
import com.raimbekov.pizza.feature.pizza.domain.PizzaInteractor
import com.raimbekov.pizza.feature.pizza.domain.PizzaInteractorImpl
import com.raimbekov.pizza.feature.pizza.domain.repository.PizzaRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
interface PizzaModule {

    @Singleton
    @Binds
    fun bindPizzaInteractor(interactor: PizzaInteractorImpl): PizzaInteractor

    @Singleton
    @Binds
    fun bindPizzaRepositoryImpl(repository: PizzaRepositoryImpl): PizzaRepository

    @Singleton
    @Binds
    fun bindPizzaService(service: PizzaServiceImpl): PizzaService

    companion object {
        @Singleton
        @Provides
        fun provideHttpClient(): HttpClient = ktorHttpClient
    }
}