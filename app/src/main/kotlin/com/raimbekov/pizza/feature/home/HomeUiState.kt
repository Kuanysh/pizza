package com.raimbekov.pizza.feature.home

import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.pizza.model.Flavor

data class HomeUiState(
    val twoFlavor: Boolean = false,
    val firstFlavor: Flavor? = null,
    val secondFlavor: Flavor? = null,
    val totalPrice: Money = Money(0),
    val orderEnabled: Boolean = false
)