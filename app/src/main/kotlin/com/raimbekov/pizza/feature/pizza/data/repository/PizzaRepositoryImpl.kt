package com.raimbekov.pizza.feature.pizza.data.repository

import com.raimbekov.pizza.data.Data
import com.raimbekov.pizza.feature.pizza.data.PizzaService
import com.raimbekov.pizza.feature.pizza.domain.repository.PizzaRepository
import com.raimbekov.pizza.feature.pizza.model.Flavor
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PizzaRepositoryImpl @Inject constructor(
    private val pizzaService: PizzaService
) : PizzaRepository {

    override fun observeFlavors(): Flow<Data<List<Flavor>>> =
        flow {
            emit(Data.Loading())
            try {
                emit(Data.Content(pizzaService.getFlavors()))
            } catch (e: Exception) {
                emit(Data.Error(e))
            }
        }
}