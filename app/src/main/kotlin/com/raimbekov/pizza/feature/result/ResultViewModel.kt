package com.raimbekov.pizza.feature.result

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.raimbekov.pizza.feature.order.domain.OrderInteractor
import com.raimbekov.pizza.feature.order.model.Order
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ResultViewModel @Inject constructor(
    private val orderInteractor: OrderInteractor,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val orderId: String
        get() = checkNotNull(savedStateHandle.get<String>("orderId"))

    fun getOrder(): Order = orderInteractor.getOrderDetails(orderId)
}