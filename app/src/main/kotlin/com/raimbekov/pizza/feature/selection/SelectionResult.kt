package com.raimbekov.pizza.feature.selection

import android.os.Parcelable
import com.raimbekov.pizza.feature.pizza.model.Flavor
import kotlinx.parcelize.Parcelize

@Parcelize
data class SelectionResult(
    val flavor: Flavor? = null
): Parcelable