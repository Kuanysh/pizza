package com.raimbekov.pizza.feature.pizza.domain

import com.raimbekov.pizza.data.Data
import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.pizza.domain.repository.PizzaRepository
import com.raimbekov.pizza.feature.pizza.model.Flavor
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface PizzaInteractor {
    fun observeFlavors(): Flow<Data<List<Flavor>>>
    fun getTotalPrice(twoFlavors: Boolean, first: Flavor?, second: Flavor?): Money
}

class PizzaInteractorImpl @Inject constructor(
    private val pizzaRepository: PizzaRepository
) : PizzaInteractor {
    override fun observeFlavors(): Flow<Data<List<Flavor>>> =
        pizzaRepository.observeFlavors()

    override fun getTotalPrice(twoFlavors: Boolean, first: Flavor?, second: Flavor?): Money =
        if (twoFlavors) {
            ((first?.price ?: Money(0)) + (second?.price ?: Money(0))) / 2
        } else {
            first?.price ?: Money(0)
        }
}