package com.raimbekov.pizza.feature.order.domain

import com.raimbekov.pizza.feature.order.domain.repository.OrderRepository
import com.raimbekov.pizza.feature.order.model.Order
import com.raimbekov.pizza.feature.pizza.domain.PizzaInteractor
import com.raimbekov.pizza.feature.pizza.model.Pizza
import javax.inject.Inject

interface OrderInteractor {
    fun orderPizza(pizza: Pizza): Order
    fun getOrderDetails(orderId: String): Order
}

class OrderInteractorImpl @Inject constructor(
    private val pizzaInteractor: PizzaInteractor,
    private val orderRepository: OrderRepository
) : OrderInteractor {

    override fun orderPizza(pizza: Pizza): Order {
        val price = when (pizza) {
            is Pizza.SingleFlavor -> pizzaInteractor.getTotalPrice(false, pizza.flavor, null)
            is Pizza.TwoFlavor -> pizzaInteractor.getTotalPrice(
                twoFlavors = true,
                first = pizza.firstFlavor,
                second = pizza.secondFlavor
            )
        }
        return orderRepository.order(pizza, price)
    }

    override fun getOrderDetails(orderId: String): Order =
        orderRepository.getOrderDetails(orderId)
}