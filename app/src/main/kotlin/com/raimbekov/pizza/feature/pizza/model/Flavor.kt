package com.raimbekov.pizza.feature.pizza.model

import android.os.Parcelable
import com.raimbekov.pizza.feature.money.model.Money
import kotlinx.parcelize.Parcelize

@Parcelize
data class Flavor(
    val name: String,
    val price: Money
) : Parcelable