package com.raimbekov.pizza.feature.order.model

import com.raimbekov.pizza.feature.money.model.Money
import com.raimbekov.pizza.feature.pizza.model.Pizza

data class Order(
    val orderId: String,
    val pizza: Pizza,
    val price: Money
)