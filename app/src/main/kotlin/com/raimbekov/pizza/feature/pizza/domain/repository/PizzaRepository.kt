package com.raimbekov.pizza.feature.pizza.domain.repository

import com.raimbekov.pizza.data.Data
import com.raimbekov.pizza.feature.pizza.model.Flavor
import kotlinx.coroutines.flow.Flow

interface PizzaRepository {
    fun observeFlavors(): Flow<Data<List<Flavor>>>
}