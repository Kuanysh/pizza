package com.raimbekov.pizza.feature.pizza.data

import com.raimbekov.pizza.feature.pizza.data.dto.FlavorDto
import com.raimbekov.pizza.feature.pizza.data.dto.toDomain
import com.raimbekov.pizza.feature.pizza.model.Flavor
import io.ktor.client.*
import io.ktor.client.request.*
import kotlinx.coroutines.delay
import javax.inject.Inject

interface PizzaService {
    suspend fun getFlavors(): List<Flavor>
}

class PizzaServiceImpl @Inject constructor(private val client: HttpClient) : PizzaService {

    override suspend fun getFlavors(): List<Flavor> {
        delay(2000) // artificial delay
        return client.get<List<FlavorDto>>("https://static.mozio.com/mobile/tests/pizzas.json")
            .map { it.toDomain() }
    }
}