package com.raimbekov.pizza.feature.home

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun HomeScreen(
    viewModel: HomeViewModel,
    onFirstFlavorSelectClicked: () -> Unit,
    onSecondFlavorSelectClicked: () -> Unit,
    onOrderClicked: (String) -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar {
                Row(Modifier.padding(16.dp)) {
                    Text(text = "Select Pizza")
                }
            }
        }
    ) {
        HomeScreenContent(
            viewModel,
            onFirstFlavorSelectClicked,
            onSecondFlavorSelectClicked,
            onOrderClicked
        )
    }
}

@Composable
private fun HomeScreenContent(
    viewModel: HomeViewModel,
    onFirstFlavorSelectClicked: () -> Unit,
    onSecondFlavorSelectClicked: () -> Unit,
    onOrderClicked: (String) -> Unit
) {
    val homeUiState = viewModel.homeUiState.collectAsState().value
    Column {
        PizzaTypeSwitch(viewModel, homeUiState.twoFlavor)
        Spacer(modifier = Modifier.height(16.dp))
        FirstFlavorInfo(homeUiState, onFirstFlavorSelectClicked)
        if (homeUiState.twoFlavor) {
            Spacer(modifier = Modifier.height(16.dp))
            SecondFlavorInfo(homeUiState, onSecondFlavorSelectClicked)
        }
        Spacer(modifier = Modifier.height(32.dp))
        TotalPrice(homeUiState)
        Spacer(modifier = Modifier.height(32.dp))
        OrderButton(viewModel, homeUiState, onOrderClicked)
    }
}

@Composable
private fun PizzaTypeSwitch(
    viewModel: HomeViewModel,
    twoFlavor: Boolean
) {
    Row {
        Text(
            text = "Two flavor pizza",
            modifier = Modifier.weight(1f)
        )
        Switch(
            checked = twoFlavor,
            onCheckedChange = { viewModel.setTwoFlavorTypeSelected(it) },
            modifier = Modifier.padding(16.dp)
        )
    }
}

@Composable
private fun FirstFlavorInfo(
    homeUiState: HomeUiState,
    onFlavorSelectClicked: () -> Unit,
) {
    val flavor: String = homeUiState.firstFlavor?.name.orEmpty()
    Column {
        Text(text = "Flavor $flavor")
        Button(
            onClick = { onFlavorSelectClicked() }
        ) {
            Text(text = "Select")
        }
    }
}

@Composable
private fun SecondFlavorInfo(
    homeUiState: HomeUiState,
    onFlavorSelectClicked: () -> Unit,
) {
    val flavorName: String = homeUiState.secondFlavor?.name.orEmpty()
    Column {
        Text(text = "Flavor $flavorName")
        Button(
            onClick = { onFlavorSelectClicked() }
        ) {
            Text(text = "Select")
        }
    }
}

@Composable
private fun TotalPrice(homeUiState: HomeUiState) {
    Text(text = "Total Price : ${homeUiState.totalPrice.print()}")
}

@Composable
private fun OrderButton(
    viewModel: HomeViewModel,
    homeUiState: HomeUiState,
    onOrderClicked: (String) -> Unit
) {
    Button(
        enabled = homeUiState.orderEnabled,
        onClick = {
            val orderId = viewModel.order()
            onOrderClicked(orderId)
        }
    ) {
        Text(text = "Order")
    }
}